<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.11.16
 * Time: 13:02
 */

namespace Fitatu\ShopBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Fitatu\ShopBundle\Entity\Orders;
use Fitatu\ShopBundle\Form\OrdersType;

/**
 * Class OrderService
 * @package Fitatu\ShopBundle\Services
 */
class OrderService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var FormFactoryInterface
     */
    protected $ff;

    /**
     * @var string
     */
    public $productRepositoryName = 'FitatuShopBundle:Orders';

    /**
     * OrderService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, FormFactoryInterface $ff)
    {
        $this->em = $em;
        $this->ff = $ff;
    }

    /**
     * @return FormInterface
     */
    public function createForm() : FormInterface
    {
        $orders = new Orders();

        return $this->ff->create(OrdersType::class, $orders);
    }

}