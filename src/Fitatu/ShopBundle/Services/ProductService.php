<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.11.16
 * Time: 0:50
 */

namespace Fitatu\ShopBundle\Services;

use Doctrine\ORM\EntityManager;
use Fitatu\ShopBundle\Entity\Tax;

class ProductService
{
    /**
     * @var EntityManager
     */
    protected $em;

    public $productRepositoryName = 'FitatuShopBundle:Product';

    /**
     * ProductService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Function selects products for the main page
     * @return mixed
     */
    public function getMainPageProducts() : array
    {
        $products = $this->em
            ->getRepository($this->productRepositoryName)
            ->getProductsForMainPage();

        return $this->setProductTax($products);
    }

    /**
     * @param array $products
     * @return array
     */
    public function setProductTax(array $products) : array
    {
        $productTax = (new Tax())->getGoodsTax('eu');
        foreach ($products as $product) {
            $price = (($product->getProductPrice() / 100) * $productTax) + $product->getProductPrice();
            $product->setProductTaxedPrice($price);
        }

        return $products;
    }
}