<?php

namespace Fitatu\ShopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package Fitatu\ShopBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * Index action
     * @return Response
     */
    public function indexAction() : Response
    {
        $productService = $this->get('fitatu_shop.product_service');
        $orderService   = $this->get('fitatu_shop.order_service');

        return $this->render('FitatuShopBundle:Default:index.html.twig', [
            'products' => $productService->getMainPageProducts(),
            'form'     => $orderService->createForm()->createView()
        ]);
    }
}
