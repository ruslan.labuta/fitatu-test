<?php

namespace Fitatu\ShopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Fitatu\ShopBundle\Entity\Tax;



/**
 * Class OrdersType
 * @package Fitatu\ShopBundle\Form
 */
class OrdersType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('orderDetails', HiddenType::class)
            ->add('delivery', CheckBoxType::class, [
                'mapped' => false,
                // Its not really correct but possible
                'value'  => (new Tax())->getDeliveryTax('eu'),
                'label'  => 'Delivery tax'
            ]);
        // Last time i forgot about Data transformers :-)))
        $builder->get('orderDetails')
            ->addModelTransformer(new CallbackTransformer(
                function ($orderDetailsAsArray) {
                    // transform the array to a string
                    return implode(', ', $orderDetailsAsArray);
                },
                function ($orderDetailsAsString) {
                    // transform the string back to an array
                    return explode(', ', $orderDetailsAsString);
                }
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fitatu\ShopBundle\Entity\Orders',
            'allow_extra_fields' => true
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fitatu_shopbundle_orders';
    }

}
