<?php

namespace Fitatu\ShopBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ProductRepository
 * @package Fitatu\ShopBundle\Repository
 */
class ProductRepository extends EntityRepository
{

    /**
     * @param int $limit
     * @return array
     */
    public function getProductsForMainPage(int $limit = 10) : array
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.createdAt', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
