<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.11.16
 * Time: 14:31
 */
namespace Fitatu\ShopBundle\Entity;

class Tax
{
    /**
     * @var array
     */
    public $deliveryTax;

    /**
     * @var array
     */
    public $goodsTax;

    /**
     * @var string
     */
    public $defaultZone = 'eu';

    /**
     * Tax constructor.
     */
    public function __construct()
    {
        // Its simple but  here can be Array Collection
        // and off course more complicated relations (Zone, Delivery, TypeTax)
        $this->deliveryTax = [
            'eu' => 100
        ];

        // percent
        $this->goodsTax = [
            'eu' => 9
        ];
    }


    /**
     * @param string $zone
     * @return int
     */
    public function getDeliveryTax(string $zone) : int
    {
        if (!array_key_exists($zone, $this->deliveryTax)) {
            $zone = $this->defaultZone;
        }
        return $this->deliveryTax[$zone];
    }

    /**
     * @param string $zone
     * @return int
     */
    public function getGoodsTax(string $zone) : int
    {
        if (!array_key_exists($zone, $this->goodsTax)) {
            $zone = $this->defaultZone;
        }
        return $this->goodsTax[$zone];
    }
}