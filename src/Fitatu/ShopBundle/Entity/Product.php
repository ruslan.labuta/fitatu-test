<?php

namespace Fitatu\ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Product
 * @package Fitatu\ShopBundle\Entity
 */
class Product
{
    /**
     * @var int
     */
    private $productId;

    /**
     * @var string
     */
    private $productName;

    /**
     * @var string
     */
    private $productImage;

    /**
     * @var string
     */
    private $productPrice;

    /**
     * @var string
     */
    private $productTaxedPrice;

    /**
     * @var string
     */
    private $productDescription;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime('now'));
    }


    /**
     * Set productId
     *
     * @param integer $productId
     * @return Product
     */
    public function setProductId(int $productId) : Product
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer
     */
    public function getProductId() : int
    {
        return $this->productId;
    }

    /**
     * Set productName
     *
     * @param string $productName
     * @return Product
     */
    public function setProductName(string $productName) : Product
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * Get productName
     *
     * @return string
     */
    public function getProductName() : string
    {
        return $this->productName;
    }

    /**
     * Set productImage
     *
     * @param string $productImage
     * @return Product
     */
    public function setProductImage(string $productImage) : Product
    {
        $this->productImage = $productImage;

        return $this;
    }

    /**
     * Get productImage
     *
     * @return string
     */
    public function getProductImage() : string
    {
        return $this->productImage;
    }

    /**
     * Set productPrice
     *
     * @param string $productPrice
     * @return Product
     */
    public function setProductPrice(string $productPrice) : Product
    {
        $this->productPrice = $productPrice;

        return $this;
    }

    /**
     * Get productTaxedPrice
     *
     * @return string
     */
    public function getProductTaxedPrice() : string
    {
        return $this->productTaxedPrice;
    }

    /**
     * Set productTaxedPrice
     *
     * @param string $productPrice
     * @return Product
     */
    public function setProductTaxedPrice(string $productTaxedPrice) : Product
    {
        $this->productTaxedPrice = $productTaxedPrice;

        return $this;
    }

    /**
     * Get productPrice
     *
     * @return string
     */
    public function getProductPrice() : string
    {
        return $this->productPrice;
    }


    /**
     * Set productDescription
     *
     * @param string $productDescription
     * @return Product
     */
    public function setProductDescription(string $productDescription) : Product
    {
        $this->productDescription = $productDescription;

        return $this;
    }

    /**
     * Get productDescription
     *
     * @return string
     */
    public function getProductDescription() : string
    {
        return $this->productDescription;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Product
     */
    public function setCreatedAt(\DateTime $createdAt) : Product
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() : \DateTime
    {
        return $this->createdAt;
    }
}
