<?php

namespace Fitatu\ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 */
class Orders
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var array
     */
    private $orderDetails;

    /**
     * @var \DateTime
     */
    private $createdAt;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime('now'));
        $this->setOrderDetails([]);
    }


    /**
     * Set orderId
     *
     * @param integer $orderId
     * @return Orders
     */
    public function setOrderId($orderId) : Orders
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer
     */
    public function getOrderId() : int
    {
        return $this->orderId;
    }

    /**
     * Set orderDetails
     *
     * @param array $orderDetails
     * @return Orders
     */
    public function setOrderDetails(array $orderDetails) : Orders
    {
        $this->orderDetails = $orderDetails;

        return $this;
    }

    /**
     * Get orderDetails
     *
     * @return array
     */
    public function getOrderDetails() : array
    {
        return $this->orderDetails;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Orders
     */
    public function setCreatedAt($createdAt) : Orders
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() : \DateTime
    {
        return $this->createdAt;
    }
}
