/**
 * Created by ruslan on 19.11.16.
 */

var basket = {
    showModal: function(){
        $('.show-modal').click(function(){
            $body  = $('#myModal .modal-body');
            $price = $(this).data('price');
            $name  = $(this).data('name');
            $id    = $(this).data('id');

            $body.append(basket.orderHtml($price, $name, $id));

            $('#myModal').modal();
            $('#myModal').on('hidden.bs.modal', function () {
                $body.html('');
                if($('#fitatu_shopbundle_orders_delivery').prop('checked')) {

                    $('#fitatu_shopbundle_orders_delivery').prop('checked', 0);
                }

            });

            $('#fitatu_shopbundle_orders_delivery').click(function(){
                $taxedPrice = $price;

                if($(this).prop('checked')) {
                    $taxedPrice = parseInt($(this).val()) + $price;
                }
                $('.price', $body).html($taxedPrice.toPrecision(4));
            })

            $('#myModal .create-order').click(function(){
                $body.html('Thank you for your order, we will contact as soon as possible')
                $(this).remove();
            })
        })

    },
    orderHtml:function($price, $name, $id){
        $html = '<div class="row" data-id="'+$id+'">';
        $html +='<div class="col-md-8">'+$name+'</div>';
        $html +='<div class="col-md-4 price">'+$price+'</div>';
        $html +='</div>';

        return $html;
    },
    init: function(){
        this.showModal();
    }
};