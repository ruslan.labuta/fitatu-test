<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.11.16
 * Time: 10:27
 */

namespace FitatuShopBundle\Tests\Entity;

use Fitatu\ShopBundle\Entity\Orders;

/**
 * Class OrderFunctionalTest
 * @package FitatuShopBundle\Tests\Entity
 */
class OrderFunctionalTest extends \PHPUnit_Framework_TestCase
{
    public $order;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $this->order = new Orders();
    }

    /**
     * @dataProvider orderDataProvider
     */
    public function testSetFunctionsOrderData($orderDetails, $createdAt)
    {
        $this->order->setOrderDetails($orderDetails);
        $this->order->setCreatedAt($createdAt);

        $this->assertEquals($orderDetails, $this->order->getOrderDetails());
        $this->assertEquals($createdAt, $this->order->getCreatedAt());
    }

    public function orderDataProvider()
    {
        return [
            [
                'orderDetails' => ['productId' => 1, 'delivery' => true, 'price' => 1000],
                'createdAt'    => new \DateTime('now')
            ]
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        unset($this->order);
    }
}
