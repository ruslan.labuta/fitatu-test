<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.11.16
 * Time: 10:27
 */

namespace FitatuShopBundle\Tests\Entity;

use Fitatu\ShopBundle\Entity\Tax;

/**
 * Class OrderFunctionalTest
 * @package FitatuShopBundle\Tests\Entity
 */
class TaxFunctionalTest extends \PHPUnit_Framework_TestCase
{
    public $tax;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $this->tax = new Tax();
    }

    /**
     * @dataProvider taxDataProvider
     */

    public function testGetFunctionsData($delivery, $goods, $zone)
    {
        $this->assertEquals($delivery, $this->tax->getDeliveryTax($zone));
        $this->assertEquals($goods, $this->tax->getGoodsTax($zone));
    }

    public function taxDataProvider()
    {
        return [
            [100, 9, 'eu'],
            [100, 9, 'en'],
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        unset($this->order);
    }
}