<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.11.16
 * Time: 10:27
 */

namespace FitatuShopBundle\Tests\Entity;

use Fitatu\ShopBundle\Entity\Product;

/**
 * Class ProductFunctionalTest
 * @package FitatuShopBundle\Tests\Entity
 */
class ProductFunctionalTest extends \PHPUnit_Framework_TestCase
{
    public $product;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $this->product = new Product();
    }

    /**
     * @dataProvider productDataProvider
     */
    public function testSetFunctionsProductData($productName, $productImage, $productDescription)
    {
        $this->product->setProductName($productName);
        $this->product->setProductImage($productImage);
        $this->product->setProductdescription($productDescription);

        $this->assertEquals($productName, $this->product->getProductName());
        $this->assertEquals($productImage, $this->product->getProductImage());
        $this->assertEquals($productDescription, $this->product->getProductDescription());
    }

    public function productDataProvider()
    {
        return [
            [
                'productName'        => 'Test product1',
                'productImage'       => '/img/test.jpg',
                'productDescription' => 'Test product1 desc',
            ]
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        unset($this->product);
    }
}

