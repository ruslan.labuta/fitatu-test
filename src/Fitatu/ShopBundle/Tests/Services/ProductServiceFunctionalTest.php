<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.11.16
 * Time: 10:27
 */

namespace FitatuShopBundle\Tests\Services;

use Fitatu\ShopBundle\Services\ProductService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


/**
 * Class ProductFunctionalTest
 * @package FitatuShopBundle\Tests\Entity
 */
class ProductServiceFunctionalTest extends KernelTestCase
{
    public $productService;
    public $em;

    function setUp()
    {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->productService = new ProductService($this->em);
    }

    /**
     * @dataProvider productServiceData
     */

    public function testMainProductFunction($count)
    {
        $this->assertGreaterThanOrEqual($count, $this->productService->getMainPageProducts());
    }

    public function productServiceData()
    {
        return [[1],[5]];
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
        unset($this->productService);
    }

}