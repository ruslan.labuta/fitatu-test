<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.11.16
 * Time: 10:08
 */
namespace FitatuShopBundle\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OrderRepositoryFunctionalTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    public function testTryToFindOrders()
    {
        $product = $this->em
            ->getRepository('FitatuShopBundle:Orders')
            ->findByOrderId(0);

        $this->assertGreaterThanOrEqual(0, $product);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}