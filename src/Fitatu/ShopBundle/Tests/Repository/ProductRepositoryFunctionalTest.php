<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.11.16
 * Time: 10:08
 */
namespace FitatuShopBundle\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductRepositoryFunctionalTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    public function testFindProductForMainPage()
    {
        $product = $this->em
            ->getRepository('FitatuShopBundle:Product')
            ->getProductsForMainPage();

        $this->assertGreaterThanOrEqual(1, $product);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}