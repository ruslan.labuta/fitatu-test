<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161119182557 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("
              CREATE TABLE `orders` (
                `order_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `order_details` TEXT NOT NULL COLLATE 'utf8_unicode_ci',
                `created_at` DATETIME NULL DEFAULT NULL,
                PRIMARY KEY (`order_id`)
            )
            COLLATE='utf8_unicode_ci'
            ENGINE=InnoDB
        ");
        $this->addSql("
             CREATE TABLE `product` (
                `product_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `product_name` VARCHAR(100) NOT NULL COLLATE 'utf8_unicode_ci',
                `product_image` VARCHAR(100) NOT NULL COLLATE 'utf8_unicode_ci',
                `product_price` DECIMAL(10,0) NOT NULL DEFAULT '0',
                `product_description` TEXT NOT NULL COLLATE 'utf8_unicode_ci',
                `created_at` DATETIME NULL DEFAULT NULL,
                PRIMARY KEY (`product_id`)
            )
            COLLATE='utf8_unicode_ci'
            ENGINE=InnoDB
        ");
        $this->addSql("INSERT INTO `product` (`product_id`, `product_name`, `product_image`, `product_price`, `product_description`, `created_at`) VALUES
	(1, 'Product -1 ', 'product-1.png', 38, 'This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '2016-11-19 02:06:34'),
	(2, 'Product -2 ', 'product-1.png', 45, 'This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '2016-11-19 02:06:34'),
	(3, 'Product -3', 'product-1.png', 78, 'This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '2016-11-19 02:06:34'),
	(4, 'Product -4', 'product-1.png', 128, 'This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '2016-11-19 02:06:34'),
	(5, 'Product -5', 'product-1.png', 56, 'This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '2016-11-19 02:06:34'),
	(6, 'Product -6', 'product-1.png', 67, 'This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '2016-11-19 02:06:34');");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DROP table product');
        $this->addSql('DROP table orders');
    }
}
