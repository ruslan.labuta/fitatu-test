Symfony Standard Edition (Fitatu test project)
========================

Welcome to the Symfony Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation]

1. You have to install composer (https://getcomposer.org/doc/00-intro.md)
2. Input command 
 ('composer install')
   in th root of your project
3. You have to install bower http://symfony.com/doc/current/frontend/bower.html, follow instruction and install bootstrap using command

bower install --save bootstrap

(.bowerrc - already created)

4. Apply migrations
 - php app/console doctrine:migrations:migrate
and check status
 - php app/console doctrine:migrations:status
more detailed instructions - http://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html
5. Enjoy

